.DEFAULT_GOAL := help
.PHONY: build clean help publish test

-include .env
export $(shell sed 's/=.*//' .env)

build: ## Build the package
	python setup.py sdist

clean: ## Remove generated artifacts
	rm -rf build dist miniscule.egg-info

help: ## Show this help
	@fgrep -h "##" $(MAKEFILE_LIST) | \
	fgrep -v fgrep | sed -e 's/## */##/' | column -t -s##

publish: ## Publish the package to PyPI
	@twine upload dist/* --repository-url https://upload.pypi.org/legacy/ \
						 -u ${PYPI_USER} -p ${PYPI_PASSWORD} \
						 --skip-existing

test: ## Test the package
	@python setup.py pytest
